<?php
function tentukan_nilai($number)
{
    //     $nilai = '';
    //     ($number >= 85 && $number <= 100) ?
    //     $nilai =  'Sangat Baik':
    //     ($number < 85 )?
    //     $nilai =  'Baik' :
    //     ($number < 70) ?
    //     $nilai =  'Cukup':
    //     $number < 60 && $number >= 0 ?
    //     $nilai =  'Kurang':
    //     $nilai =  'Invalid Number'
    // return $nilai ;

    if ($number >= 85 && $number <= 100) {
        return "$number (Sangat Baik)";
    } elseif ($number >= 70 && $number < 85) {
        return "$number (Baik)";
    } elseif ($number >= 60 && $number < 70) {
        return "$number (Cukup)";
    } else {
        return "$number (kurang)";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo '<br>';
echo tentukan_nilai(76); //Baik
echo '<br>';
echo tentukan_nilai(67); //Cukup
echo '<br>';
echo tentukan_nilai(43); //Kurang
